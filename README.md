# jmr-bot

A Clojure library designed to ... well, that part is up to you.

## Installation

sudo apt install openjdk-11-jdk python3-praw festival

## Running

From inside jmr-bot directory:

lein run
