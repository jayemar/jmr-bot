(ns jmr-bot.reddit
  (:require [libpython-clj.require :refer [require-python]]
            [libpython-clj.python :refer [py. py.. py.-] :as py]
            [clojure.string :as str]
            [clojure.set :refer [intersection]]
            [jmr-bot.utils :refer [ini-map]]))

;; Reference:  https://techradicals.wordpress.com/2020/08/09/how-to-create-a-simple-reddit-bot/
;; Reddit link: https://ssl.reddit.com/prefs/apps/
(require-python 'praw)

(def config-map (get (ini-map "/etc/reddit.conf") "reddit"))

(defonce reddit (praw/Reddit
                 :user_agent (get config-map "bot-name")
                 :client_id (get config-map "personal-use-script")
                 :client_secret (get config-map "secret")
                 :username (get config-map "username")
                 :password (get config-map "password")))

(defn subreddit [subreddit-name]
  (py. reddit subreddit subreddit-name))

(defn posts
  ([subreddit-name] (posts subreddit-name 10))
  ([subreddit-name n]
   (py. (subreddit subreddit-name) new :limit n)))

;; TODO: limit 1 sometimes returns nil; just guessing with 3
(defn single-post
  ([subreddit-name] (single-post subreddit-name "new"))
  ([subreddit-name & post-type]
   (first (posts subreddit-name))))

(defn arg-play [& subreddit-list]
  (println (subreddit-list)))

(defn random-post [subreddit-name & post-type]
   (py. (subreddit subreddit-name) random))

(defn post-message [subreddit-name title body-text] 
  (let [sub (py. reddit subreddit subreddit-name)]
    (py. sub submit title body-text)))

(defn post-id [post]
  (py/get-attr post "id"))

(defn title [post]
  (py/get-attr post "title"))

(defn body [post]
  (py/get-attr post "selftext"))

(defn time-posted [post]
  (.toString 
   (java.time.Instant/ofEpochSecond (py/get-attr post "created_utc"))))

(defn post-info [post]
  (println (format "%s\n\t%s\n\t%s\n"
                   (time-posted post) (title post) (body post))))
