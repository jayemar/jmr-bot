(ns jmr-bot.core
  (:require [libpython-clj.require :refer [require-python]]
            [libpython-clj.python :refer [py. py.. py.-] :as py]
            [clojure.string :as str]
            [clojure.set :refer [intersection]]
            [jmr-bot.azure :as azure]
            [jmr-bot.reddit :as reddit]
            [jmr-bot.speak :as speak]
            [jmr-bot.utils :refer [random-sleep-min]]))

;; TODO: make use of all of these subs
(def joke-subs '(jokes dadjokes cleanjokes puns))

(defn -main-stream
  ([] (-main-stream "jokes"))
  ([subreddit-name]
   (let [sub (py. reddit/reddit subreddit "jokes")
         stream (py/get-attr sub "stream")]
     (println (format "Using subreddit %s" subreddit-name))
     (doseq [post (py. stream submissions)] (reddit/post-info post)))))

(defn- infinite-new-posts [subreddit-name]
  42)

(def temp-filename "/tmp/speech.wav")

(defn -main
  ([] (-main "jokes"))
  ([subreddit-name]
   (println (format "Using subreddit %s" subreddit-name))
   (loop [post (reddit/single-post subreddit-name)
          post-ids #{}]
     (let [post-id (reddit/post-id post)
           min-wait 0.5
           max-wait 7.0]
       (if-not (contains? post-ids post-id)
         (do 
           (reddit/post-info post)
           (-> post
               speak/prepare-joke
               azure/tts
               (azure/save-soundbite temp-filename))
           (speak/read-file temp-filename)
           ;; (speak/read-joke post)
           (random-sleep-min min-wait max-wait)
           (recur (reddit/single-post subreddit-name) (conj post-ids post-id)))
         (do
           (random-sleep-min min-wait max-wait)
           (recur (reddit/single-post subreddit-name) post-ids)))))))

