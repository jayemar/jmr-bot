(ns jmr-bot.utils
  (:require [clojure.string :as str]))

(defn random-range [min max]
  (+ (rand (- max min)) min))

(defn random-sleep-sec [min max]
  (let [sleep-sec (random-range min max)
        sleep-millis (* sleep-sec 1000)]
  (Thread/sleep sleep-millis)))

(defn random-sleep-min [min max]
  (let [min-sec (* min 60)
        max-sec (* max 60)]
  (random-sleep-sec min-sec max-sec)))

(defn ini-heading
  "Returns portion between square brackets if heading, nil otherwise"
  [s]
  (let [rex (re-find #"\[(.*)\]" s)]
    (if rex (second rex))))

(defn ini-kv
  "Returns key/value if appropriate, nil otherwise"
  [s]
  (let [rex (re-find #"=" s)]
    (if rex (apply hash-map (str/split s #"=")))))

(defn check-line [s]
  (let [head (ini-heading s)]
    (if head
      head
      (let [kv (ini-kv s)]
        (if kv kv)))))

(defn map-keywords
  "Convert the keys of a map to keywords"
  [m]
  (zipmap (map keyword (keys m)) (vals m)))

;; NOTE: Make sure you pass in the reversed vector
(defn thing-func [mix coll currmaps & [kw?]]
  (let [line (first mix)
        others (rest mix)
        kw-func (if kw? keyword identity)
        mp-func (if kw? map-keywords identity)]
    (cond (empty? mix) coll
          (map? line) (thing-func others coll (conj (mp-func currmaps) line) kw?)
          (string? line) (thing-func others (assoc-in coll [(kw-func line)] currmaps) {} kw?))))

;; TODO: This is an incomplete function as it only handles a single key
(defn ini-map [filename & [keywords]]
  (let [ini-str (slurp filename)
        ini-vec (str/split-lines ini-str)
        ini-mix (map check-line ini-vec)
        ini-rev (reverse ini-mix)]
    (thing-func ini-rev {} {} keywords)))

