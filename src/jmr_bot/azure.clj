(ns jmr-bot.azure
  (:require [clj-http.client :as client]
            [clojure.java.io :as io]
            [cheshire.core :as ches]
            [jmr-bot.utils :as utils]))

(comment
  (require '[clojure.repl :refer [doc]]))

;; - REFERENCE -
;; https://docs.microsoft.com/en-us/azure/cognitive-services/speech-service/overview

(def config (get (utils/ini-map "/etc/azure.conf") "azure"))

(def key1 (get config "key1"))

(def key2 (get config "key2"))

;; https://eastus.api.cognitive.microsoft.com/sts/v1.0/issuetoken
;; https://eastus.tts.speech.microsoft.com/cognitiveservices/voices/list
(def base-api-url (format "https://%s.api.cognitive.microsoft.com" (get config "region")))
(def base-tts-url (format "https://%s.tts.speech.microsoft.com" (get config "region")))

(def token-url (str base-api-url "/sts/v1.0/issuetoken"))
(def tts-url (str base-tts-url "/cognitiveservices/v1"))
(def voice-query-url (str base-tts-url "/cognitiveservices/voices/list"))

;; curl --location --request GET 'https://INSERT_ENDPOINT_HERE.tts.speech.microsoft.com/cognitiveservices/voices/list' \
;; --header 'Ocp-Apim-Subscription-Key: INSERT_SUBSCRIPTION_KEY_HERE'

(comment
  raw-16khz-16bit-mono-pcm
  raw-8khz-8bit-mono-mulaw
  riff-8khz-8bit-mono-alaw
  riff-8khz-8bit-mono-mulaw
  riff-16khz-16bit-mono-pcm
  audio-16khz-128kbitrate-mono-mp3
  audio-16khz-64kbitrate-mono-mp3
  audio-16khz-32kbitrate-mono-mp3
  raw-24khz-16bit-mono-pcm
  riff-24khz-16bit-mono-pcm
  audio-24khz-160kbitrate-mono-mp3
  audio-24khz-96kbitrate-mono-mp3
  audio-24khz-48kbitrate-mono-mp3
  ogg-24khz-16bit-mono-opus)

(def simple-headers {"Ocp-Apim-Subscription-Key" key1})
(def post-headers
  (merge simple-headers {"Content-Type" "application/ssml+xml"
                         "X-Microsoft-OutputFormat" "riff-24khz-16bit-mono-pcm"}))

(defn tts-headers [bearer]
  (merge post-headers {"Authorization" (format "Bearer %s" bearer)}))

(defn ssml [s]
  (format 
"<speak version=\"1.0\" xml:lang=\"en-US\">
  <voice xml:lang=\"en-US\" xml:gender=\"Female\" name=\"en-US-AriaRUS\">
    %s
  </voice>
</speak>" s))

(defn url-test []
  (client/get tts-url {:headers {"Ocp-Apim-Subscription-Key" key1} :as :json}))

(defn get-token []
  (client/post token-url {:headers simple-headers}))

(defn tts
  "`s` is the string that should be spoken."
  [s]
  (let [ssml-str (ssml s)
        token (get-token)]
    (assert (= (:status token) 200))
    (let [resp (client/post
                tts-url {:headers (tts-headers (:body token))
                         :body ssml-str
                         :as :byte-array})]
      (:body resp))))

(defn save-soundbite
  "`sound-bytes` should be the :body of the TTS response."
  [sound-bytes filename]
  (with-open [w (io/output-stream filename)]
    (.write w sound-bytes)))
