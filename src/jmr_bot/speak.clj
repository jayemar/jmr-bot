(ns jmr-bot.speak
  (:require [libpython-clj.require :refer [require-python]]
            [libpython-clj.python :refer [py. py.. py.-] :as py]
            [clojure.string :as str]))

;; https://askubuntu.com/questions/501910/how-to-text-to-speech-output-using-command-line
(defn speak-espeak [msg-string]
  (clojure.java.shell/sh "espeak" "-s" "150" "-v" "en-us" "-g" "3" msg-string))

(defn speak [msg-string]
  (clojure.java.shell/sh "festival" "--tts" :in msg-string))

(defn prepare-joke [post]
  (let [title (py/get-attr post "title")
        body (py/get-attr post "selftext")]
    (str "Incoming hilarity... " title "... " body)))

(defn read-joke [post]
  (let [title (py/get-attr post "title")
        body (py/get-attr post "selftext")]
    (speak "Incoming hilarity")
    (speak title)
    (Thread/sleep 500)
    (speak body)))

(defn read-file [filename]
  (clojure.java.shell/sh "aplay" filename))
