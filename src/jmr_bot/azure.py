#!/usr/bin/env python3

from configparser import ConfigParser
import requests

config_file = "/etc/azure.conf"

cfg = ConfigParser()
cfg.read(config_file)

region = cfg['azure']['region']
key1 = cfg['azure']['key1']

base_api_url = "https://{}.api.cognitive.microsoft.com".format(region)
base_tts_url = "https://{}.tts.speech.microsoft.com".format(region)
token_url = base_api_url + "/sts/v1.0/issuetoken"
tts_url = base_tts_url + "/cognitiveservices/v1"
voice_query_url = base_tts_url + "/cognitiveservices/voices/list"

simple_headers = {"Ocp-Apim-Subscription-Key": key1}
post_headers = {**simple_headers,
                **{"Content-Type": "application/ssml+xml",
                   "X-Microsoft-OutputFormat": "riff-24khz-16bit-mono-pcm"}}

def get_token():
    resp = requests.post(token_url, headers=simple_headers)
    if resp.ok:
        return resp.content
    resp.raise_for_status()


def _get_tts_headers(bearer_token):
    return {**post_headers,
            **{"Authorization": "Bearer {}".format(bearer_token)}}


def _build_ssml(words):
    return """
    <speak version=\"1.0\" xml:lang=\"en-US\">
      <voice xml:lang=\"en-US\" xml:gender=\"Female\" name=\"en-US-AriaRUS\">
        {} 
      </voice>
    </speak>""".format(words)

def request_speech(words):
    token = get_token()
    headers = _get_tts_headers(token)
    ssml = _build_ssml(words)
    resp = requests.post(tts_url, headers=headers, data=ssml)
    if resp.ok:
        return resp.content
    resp.raise_for_status()


if __name__ == '__main__':
    speech = request_speech("In the time of chimpazees")
